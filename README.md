# Makefiles
- [cpp](cpp/makefile)
    - Compile cpp and c
- [fortran](fortran/makefile)
    - Compiles all fortran files
    - Easy example
- [fortran2](fortran2/makefile)
    - Compiles Fortran and C and links it together
    - Contains also Fortran-Unittests
- [documents](documents/makefile)
    - Easy example to convert documents
- [tex_and_documents](tex_and_documents/makefile)
    - Compiles TeX, LibreOffice (`.odg`), Dia and Markdown
    - It could be used within [this docker image](https://gitlab.com/Bergiu/dockeroffice)
- [tex](tex/makefile)
    - Trivial example of a makefile

## How to:
- Other examples could be found in the folders. I recommend reading `tex/makefile` and `documents/makefile` first.

```makefile
hello:
    echo "hello world"
```

- `make hello` would execute `echo "hello world"`
- `hello` is called a target or rule
- targets could just execute commands or could be used to create files

```makefile
MD_TARGETS = a.pdf b.pdf c.pdf

%.pdf: %.md
    pandoc --from-markdown --to=pdf -t latex $< -o $@

convert: $(MD_TARGETS)
```

- `make convert` converts `a.md`, `b.md` and `c.md` to `a.pdf`, `b.pdf` and `c.pdf`
- `convert` target has the dependencies from the variable `$(MD_TARGETS)`, which are: `a.pdf`, `b.pdf` and `c.pdf`
- To create the dependencies there is the rule: `%.pdf:`
- `$<` refers to the current dependency, for example `a.md`
- `$@` refers to the target, for example `a.pdf`
- `%` is a placeholder for a name
- `MD_TARGETS = ...` is a variable that gets some values
- `$(MD_TARGETS)` is to get the value of the variable

## Special Characters:
- `$@`
    - left site/target
- `$^`
    - full right site/dependencies
- `$<`
    - first item on the right site/first dependency
- `$(@D)`
    - Directory where the current file is located

## Notices:
- `target: dependency1 dependency2`
    - A target with two dependencies
- `%.pdf: %.md`
    - A target that creates a pdf from an markdown
    - `%` is a placeholder for an variable name, that is the same on the left and right side
    - `a.pdf` in this case has the dependency `a.md`
- `VARIABLE ?= "Value"`
    - Default value, but can be changed from env vars
    - `VARIABLE="BLA" make` would execute make and replace the value of the variable with `"BLA"`
- `.PHONY: build convert hello install clean`
    - All Targets that don't create a file called like the target
- `OBJS = $(patsubst %,$(DIR)/%,$(_OBJS))`
    - Rename every item in `_OBJS` and add a directory name before the name
- `MD_SRCS=$(shell find -type f -iname "*.md")`
    - Find all `.md` files and save them into `MD_SRCS` variable
- `MD_OBJS=$(MD_SRCS:.md=.pdf)`
    - Replace `.md` with `.pdf` in each filename and save it in `MD_OBJS` variable

## Snippets
```makefile
MD_SRCS=$(shell find -type f -iname "*.md")
MD_OBJS=$(MD_SRCS:.md=.pdf)
```
- Find all `.md` files and save them into `MD_SRCS` variable
- Replace `.md` with `.pdf` in each filename and save it in `MD_OBJS` variable
- Example in tex_and_documents

```makefile
DIR = obj
_OBJS = a.o b.o c.o d.o
OBJS = $(patsubst %,$(DIR)/%,$(_OBJS))
```
- Rename every item in `_OBJS` and add a directory name before the name
- The result is: `obj/a.o` `obj/b.o` `obj/c.o` 
- Example in fortran
